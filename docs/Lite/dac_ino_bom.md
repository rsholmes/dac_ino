# dac_ino.sch BOM

Fri 10 Dec 2021 09:24:00 PM EST

Generated from schematic by Eeschema 5.1.12-84ad8e8a86~92~ubuntu20.04.1

**Component Count:** 23

| Refs | Qty | Component | Description |
| ----- | --- | ---- | ----------- |
| A1 | 1 | Arduino_Nano_v3.x 5V | Arduino Nano v3.x |
| C1, C4 | 2 | 10uF | Polarized capacitor |
| C2 | 1 | 0.1uF | Unpolarized capacitor |
| H1, H2, H3, H4 | 4 | MountingHole | Mounting Hole without connection |
| J1 | 1 | CV OUT A | Generic connector, single row, 01x02, script generated (kicad-library-utils/schlib/autogen/connector/) |
| J2 | 1 | CV OUT B | Generic connector, single row, 01x02, script generated (kicad-library-utils/schlib/autogen/connector/) |
| J5 | 1 | GATE OUT A | Generic connector, single row, 01x02, script generated (kicad-library-utils/schlib/autogen/connector/) |
| J6, J7 | 2 | Conn_01x04 | Generic connector, single row, 01x04, script generated (kicad-library-utils/schlib/autogen/connector/) |
| J8 | 1 | VCC | Generic connector, single row, 01x02, script generated (kicad-library-utils/schlib/autogen/connector/) |
| J9 | 1 | Vin | Generic connector, single row, 01x02, script generated (kicad-library-utils/schlib/autogen/connector/) |
| Q1 | 1 | 2N3904 | 0.2A Ic, 40V Vce, Small Signal NPN Transistor, TO-92 |
| R1, R8 | 2 | 10k | Resistor |
| R2, R3, R6, R7 | 4 | 1k | Resistor |
| U1 | 1 | MCP4922 | 2-Channel 12-Bit D/A Converters with SPI Interface |
    
